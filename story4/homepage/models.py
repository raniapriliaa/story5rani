from django.db import models

class Jadwal(models.Model):
    hari = models.CharField(max_length = 10)
    tanggal = models.DateField()
    jam = models.TimeField()
    kegiatan  = models.CharField(max_length = 100)
    tempat = models.CharField(max_length = 100)
    kategori = models.CharField(max_length = 60)


